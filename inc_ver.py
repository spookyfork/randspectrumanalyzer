#! python3

version_file = open("include/version.h", "r")
lines = version_file.readlines()
version_file.close()
current_verstion = None

for i,line in enumerate(lines):
    if line.startswith("#define BUILD_NUMBER_INT"):
        code_part = line[:line.find("//")]
        comment_part = line[line.find("//"):]
        tokens = code_part.split()
        current_verstion = int(tokens[-1])
        tokens[-1] = str(current_verstion+1)
        new_line = " ".join([" ".join(tokens), comment_part])
        lines[i] = new_line
    if line.startswith("#define BUILD_NUMBER_STR"):
        code_part = line[:line.find("//")]
        comment_part = line[line.find("//"):]
        tokens = code_part.split()
        current_verstion = int(tokens[-1].strip('"'))
        tokens[-1] = '"' + str(current_verstion+1) + '"'
        new_line = " ".join([" ".join(tokens), comment_part])
        lines[i] = new_line


version_file = open("include/version.h", "w")
version_file.writelines(lines)
version_file.close()