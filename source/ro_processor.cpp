//------------------------------------------------------------------------
// Copyright(c) 2021 randOhm.
//------------------------------------------------------------------------

#include <complex>
#include <cmath>
#include <iterator>

#include <Windows.h>

#include <iostream>
#include <string>
#include <filesystem>

#include "../lib/fftw3.h"

#include "../include/ro_processor.h"
#include "../include/ro_cids.h"
#include "../include/ro_fft.h"

#include "base/source/fstreamer.h"
#include "pluginterfaces/vst/ivstparameterchanges.h"

#include "public.sdk/source/vst/utility/audiobuffers.h"
#include "public.sdk/source/vst/utility/processdataslicer.h"
#include "public.sdk/source/vst/utility/rttransfer.h"
#include "public.sdk/source/vst/utility/sampleaccurate.h"


#ifdef _DEBUG
#define new new( _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

extern Steinberg::tchar gPath[];

using namespace Steinberg;

namespace RandOhm {
	//------------------------------------------------------------------------
	// RandDelayProcessor
	//------------------------------------------------------------------------
	RandSpectrumProcessor::RandSpectrumProcessor()
	{
		buffer32 = nullptr;
		//--- set the wanted controller for our processor
		setControllerClass(kRandSpectrumControllerUID);
	}

	//------------------------------------------------------------------------
	RandSpectrumProcessor::~RandSpectrumProcessor()
	{
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumProcessor::initialize(FUnknown* context)
	{
		// Here the Plug-in will be instanciated

		//---always initialize the parent-------
		tresult result = AudioEffect::initialize(context);
		// if everything Ok, continue
		if (result != kResultOk)
		{
			return result;
		}

		if (!FFTWWrapper::loadDll()) exit(1); //TODO:Find a new place for this
		buffer32 = new RingBuffer<Steinberg::Vst::Sample32>[2]{ RingBuffer<Steinberg::Vst::Sample32>(fftSize), RingBuffer<Steinberg::Vst::Sample32>(fftSize) };

		//--- create Audio IO ------
		addAudioInput(STR16("Stereo In"), Steinberg::Vst::SpeakerArr::kStereo);
		addAudioOutput(STR16("Stereo Out"), Steinberg::Vst::SpeakerArr::kStereo);

		/* If you don't need an event bus, you can remove the next line */
		//addEventInput (STR16 ("Event In"), 1);

		return kResultOk;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumProcessor::terminate()
	{
		// Here the Plug-in will be de-instanciated, last possibility to remove some memory!
		FFTWWrapper::unloadDll(); //TODO: Find a new place for this too
		delete[] buffer32;
		//---do not forget to call parent ------
		return AudioEffect::terminate();
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumProcessor::setActive(TBool state)
	{
		//mda-vst3 sends message from here
		if (state)
		{
			Steinberg::Vst::IMessage* message = allocateMessage();
			if (message)
			{
				message->setMessageID("data");
				message->getAttributes()->setInt("LChannel", (Steinberg::int64) &(buffer32[0]));
				message->getAttributes()->setInt("RChannel", (Steinberg::int64) &(buffer32[1]));
				message->getAttributes()->setInt("Mutex", (Steinberg::int64) &(ro_mutex));
				message->getAttributes()->setInt("DataSize", bufferSize);
				message->getAttributes()->setFloat("SampleRate", processSetup.sampleRate);
				sendMessage(message);
				message->release();
			}
		}

		//--- called when the Plug-in is enable/disable (On/Off) -----
		return AudioEffect::setActive(state);
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumProcessor::process(Vst::ProcessData& data)
	{
		//--- First : Read inputs parameter changes-----------

		if (data.inputParameterChanges)
		{
			int32 numParamsChanged = data.inputParameterChanges->getParameterCount();
			for (int32 index = 0; index < numParamsChanged; index++)
			{
				if (auto* paramQueue = data.inputParameterChanges->getParameterData(index))
				{
					Vst::ParamValue value;
					int32 sampleOffset;
					int32 numPoints = paramQueue->getPointCount();
					switch (paramQueue->getParameterId())
					{
					}
				}
			}
		}
		//--- Here you have to implement your processing
		Steinberg::Vst::SpeakerArrangement arr;
		getBusArrangement(Steinberg::Vst::BusDirections::kOutput, 0, arr);
		int32 numChannels = Steinberg::Vst::SpeakerArr::getChannelCount(arr);

		if (numChannels == 0 || !data.inputs || !data.outputs) return kResultOk;

		for (int channel = 0; channel < numChannels; channel++)
		{
			float* inputBuffer = data.inputs[0].channelBuffers32[channel];
			float* outputBuffer = data.outputs[0].channelBuffers32[channel];
			std::lock_guard<std::mutex> lk(ro_mutex);
			for (int i = 0; i < data.numSamples; i++) {
				buffer32[channel].push(inputBuffer[i]);
				outputBuffer[i] = inputBuffer[i];
			}
		}

		//NOTE: The following is a ProcessDataSlicer implementation
		// It seems to be better performace than just iterating over every sample 
		// But I don't know why (it's not in a separate thread from what I can see.
		// The downside of using this is that it causes the spectrum analizer 
		// to be choppy. May be an implementation issue not a slicer problem.
		/*
		static constexpr auto SliceSize = 16u;

		Steinberg::Vst::ProcessDataSlicer slicer(SliceSize);

		auto doProcessing = [this](Steinberg::Vst::ProcessData& data) {
			auto inputs = data.inputs;
			auto outputs = data.outputs;

			for (auto channelIndex = 0; channelIndex < inputs[0].numChannels; ++channelIndex)
			{
				auto inChannelBuffer = Steinberg::Vst::getChannelBuffers<Steinberg::Vst::SymbolicSampleSizes::kSample32>(inputs[0])[channelIndex];
				auto outChannelBuffer = Steinberg::Vst::getChannelBuffers<Steinberg::Vst::SymbolicSampleSizes::kSample32>(outputs[0])[channelIndex];
				int channelBufferIndex = bufferIndex;
				for (auto sampleIndex = 0; sampleIndex < data.numSamples; ++sampleIndex)
				{
					auto sample = inChannelBuffer[sampleIndex];
					outChannelBuffer[sampleIndex] = sample;

					buffer32[channelIndex].push(sample);

					if ((int)(processSetup.sampleRate * 0.016) == channelBufferIndex) {
						buffer32[channelIndex].setStart(); //Index now points to the earliest sample so we set start here
						run_fft2(buffer32[channelIndex], 0, bufferSize);
						//TODO: test having a thread that copies that with thread safety and then waits
						//while we only wake it up from here
						{
							//std::lock_guard<std::mutex> lk(RandOhm::ro_mutex);
							for (int i = 0; i < FFT_BUFFER_SIZE; i++)
							{
								fftBuffer[channelIndex][i].real = qwe_out[i][0];
								fftBuffer[channelIndex][i].imag = qwe_out[i][1];
							}
							//memcpy(fftBuffer[channelIndex], temp1Buffer, sizeof(ComplexNumber) * bufferSize);
						}
						channelBufferIndex = 0;
					}

					channelBufferIndex++;

				}
				bufferIndex = channelBufferIndex;
			}
		};

		slicer.process<Steinberg::Vst::SymbolicSampleSizes::kSample32>(data, doProcessing);
		*/
		return kResultOk;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumProcessor::setupProcessing(Vst::ProcessSetup& newSetup)
	{

		//--- called before any processing ----
		return AudioEffect::setupProcessing(newSetup);
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumProcessor::canProcessSampleSize(int32 symbolicSampleSize)
	{
		// by default kSample32 is supported
		if (symbolicSampleSize == Vst::kSample32)
			return kResultTrue;

		// disable the following comment if your processing support kSample64
		/* if (symbolicSampleSize == Vst::kSample64)
			return kResultTrue; */

		return kResultFalse;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumProcessor::setState(IBStream* state)
	{
		// called when we load a preset, the model has to be reloaded
		IBStreamer streamer(state, kLittleEndian);

		return kResultOk;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumProcessor::getState(IBStream* state)
	{
		// here we need to save the model
		IBStreamer streamer(state, kLittleEndian);

		return kResultOk;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumProcessor::setBusArrangements(Steinberg::Vst::SpeakerArrangement* inputs, Steinberg::int32 numIns,
		Steinberg::Vst::SpeakerArrangement* outputs, Steinberg::int32 numOuts)
	{
		//if (inputs[0] != Steinberg::Vst::SpeakerArr::kStereo)
		//	return false;
		//TODO Setup buffers here

		int c = Steinberg::Vst::SpeakerArr::getChannelCount(inputs[0]);

		return AudioEffect::setBusArrangements(inputs, numIns, outputs, numOuts);
	}
	//------------------------------------------------------------------------
} // namespace RandOhm
