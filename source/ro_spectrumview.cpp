#include "../include/ro_spectrumview.h"
#include "../include/ro_cids.h"

#include "vstgui/lib/animation/timingfunctions.h"
#include <algorithm>
#include <mutex>

#ifdef _DEBUG
#define new new( _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

using namespace std::chrono_literals;

namespace RandOhm {
	Spectrum::Spectrum(const CRect& size) : VSTGUI::CControl(size)
	{
		dbMin = RAND_DBMIN_DEF;
		dbMax = RAND_DBMAX_DEF;
		freqMin = RAND_FREQMIN_DEF;
		freqMax = RAND_FREQMAX_DEF;

#ifdef REFRESH_ONIDLE
		setWantsIdle(true);
#endif // REFRESH_ONIDLE
#ifdef REFRESH_THREAD_SETDIRTY
		CView::kDirtyCallAlwaysOnMainThread = false;
#endif // REFRESH_THREAD_SETDIRTY
	}

	Spectrum::~Spectrum()
	{
#ifdef REFRESH_THREAD_SETDIRTY
		if (!threadShouldEnd)
		{
			this->threadShouldEnd = true;
			refreshThread->join();
			delete refreshThread;
		}
#endif // REFRESH_THREAD_SETDIRTY

		if (xCoords)
			delete[] xCoords;
		removeAllAnimations();
	}

	void Spectrum::draw(CDrawContext* pContext)
	{
		background->copyFrom(pContext, getViewSize());
		CDrawContext::Transform t(*pContext, CGraphicsTransform().translate(getViewSize().getTopLeft())); //TODO: test if this needs to be called every time

		double width = getWidth();
		double height = getHeight();
		pContext->setDrawMode(CDrawModeFlags::kAntiAliasing);
		VSTGUI::CLineStyle lineStyle(VSTGUI::CLineStyle::LineCap::kLineCapRound, VSTGUI::CLineStyle::LineJoin::kLineJoinBevel);
		pContext->setLineStyle(lineStyle);

		for (int i = 0; i < 2; i++) {
			if (fftB[i]->getOutBuffer())
			{
				std::vector<CPoint> points = calculatePoints(fftB[i]->getOutBuffer());
				points = filterVisible(points);

				CGraphicsPath* maxPath = pContext->createGraphicsPath();
				generateAccurateGraphicsPath(points, maxPath);

				pContext->setFrameColor(style.getStrokeForChannel(i));
				pContext->setLineWidth(2);
				pContext->setFillColor(style.getFillForChannel(i));
				//TODO: Make another path and (? forgot what this was about)
				pContext->drawGraphicsPath(maxPath, CDrawContext::kPathStroked);

				//Connect to edge points to draw the fill
				maxPath->addLine(VSTGUI::CPoint(getWidth(), getHeight()));
				maxPath->addLine(VSTGUI::CPoint(0, getHeight()));
				maxPath->closeSubpath();
				pContext->drawGraphicsPath(maxPath, CDrawContext::kPathFilled);

				maxPath->forget();
			}
		}

		currentTime = std::chrono::system_clock::now();
		std::chrono::duration<double> diff = currentTime - lastTime;
		lastTime = currentTime;
		pContext->setFontColor(style.fontColor);
		pContext->setFont(VSTGUI::kNormalFont, 20);
		debugText = std::to_string(diff.count());
		pContext->drawString(debugText.c_str(), CPoint(20, 20));

		setDirty(false);
	}

	void Spectrum::update()
	{
		if (fftB[0] && inBuffers[0]) {
			inBuffers[0]->setStart();
			fftB[0]->execute(*inBuffers[0]);
		}
		if (fftB[1] && inBuffers[1]) {
			inBuffers[1]->setStart();
			fftB[1]->execute(*inBuffers[1]);
		}
#ifdef REFRESH_THREAD_SETDIRTY
		setDirty(true);
#endif // REFRESH_THREAD_SETDIRTY
#if defined(REFRESH_ONIDLE) || defined(REFRESH_TIMER) ||  defined(REFRESH_ANIMATION)
		invalid();
#endif // REFRESH_ONIDLE | REFRESH_TIMER
	}

	bool Spectrum::removed(CView* parent)
	{
#ifdef REFRESH_THREAD_SETDIRTY
		this->threadShouldEnd = true;
		refreshThread->join();
		delete refreshThread;
		refreshThread = nullptr;
#endif // REFRESH_THREAD_SETDIRTY
#ifdef REFRESH_TIMER
		timer->stop();
#endif // REFRESH_TIMER
		return CControl::removed(parent);
	}

	void Spectrum::setViewSize(const CRect& rect, bool invalid)
	{
		CControl::setViewSize(rect, invalid);
		rescaleLevels();
	}

	bool Spectrum::attached(CView* parent)
	{
		background = COffscreenContext::create(getViewSize().getSize());
		generateBackground();
		//Animations are controlled by frame so we need to make sure our view is attached to add an animation
		//addAnimation("Cycle", new ViewDrawAnimation(), new LoopAnimationTiming());
		lastTime = std::chrono::system_clock::now();

#ifdef REFRESH_THREAD_SETDIRTY
		this->threadShouldEnd = false;
		refreshThread = new std::thread([this]() {
			while (!this->threadShouldEnd) {
				{
					std::chrono::system_clock::time_point t = std::chrono::system_clock::now();
					//NOTE: This skips a frame, but it means we are already in the process updating so it's ok
					//if (!isUpdating && !this->isDirty() && !this->isEditing())
					this->update();

					std::chrono::system_clock::time_point s = std::chrono::system_clock::now();
					//TODO: Decide what is better
					//std::this_thread::sleep_for(16ms - (s - t));
					while (std::chrono::system_clock::now() - t < 16.66666667ms) {};
				}
			}});
#endif // REFRESH_THREAD_SETDIRTY
#ifdef REFRESH_TIMER
		std::function<void(CVSTGUITimer*)> f([this](CVSTGUITimer* a) {
			//if (!isUpdating && !this->isDirty() && !this->isEditing())
			this->update();
			});
		timer = new CVSTGUITimer(f, 1, true);
#endif // REFRESH_TIMER

		bool result = CControl::attached(parent);

#ifdef REFRESH_ANIMATION
		addAnimation("RefreshLoop", new ViewDrawAnimation(), new LoopAnimationTiming()); // MUST BE CALLED AFTER CControl::attached 
#endif // REFRESH_ANIMATION

		return result;

	}

	void Spectrum::setData(RingBuffer<Steinberg::Vst::Sample32>* l, RingBuffer<Steinberg::Vst::Sample32>* r, unsigned int dataSize)
	{
		inBuffers[0] = l;
		inBuffers[1] = r;

		if (fftB[0]) delete fftB[0];
		if (fftB[1]) delete fftB[1];

		fftB[0] = new FFT(dataSize);
		fftB[1] = new FFT(dataSize);
		this->dataSize = dataSize;
	}

	void Spectrum::setSampleRate(double sampleRate)
	{
		//Sampling rate dictates the scaling on x axis
		this->sampleRate = sampleRate;
		maxFreq = sampleRate / 2;
	}

	void Spectrum::setFrequencyRange(double fMin, double fMax)
	{
		freqMin = fMin;
		freqMax = fMax;
	}

	void Spectrum::setDbRange(double dbMin, double dbMax)
	{
		this->dbMax = dbMax;
		this->dbMin = dbMin;
	}

	//This needs to be called every time we make changes to 
	void Spectrum::rescaleLevels()
	{
		logScale = 1.0 / (log10(freqMax) - log10(freqMin)) * getWidth();
		calculateXCoords();
		generateBackground();
		invalid();
	}


	void Spectrum::generateBackground()
	{
		if (!background)
			return;
		background->setDrawMode(CDrawModeFlags::kAntiAliasing);
		background->beginDraw();
		background->setFillColor(style.backgroundColor);
		background->drawRect(background->getSurfaceRect(), kDrawFilled);

		background->setLineWidth(1);
		background->setFrameColor(style.gridColor);

		double width = getWidth();
		double height = getHeight();
		double freq = 1;
		double inc = 1;
		while (freq < freqMin)
		{
			freq += inc;
			if (freq == inc * 10) {
				inc *= 10;
			}
		}
		for (; freq < maxFreq; freq += inc)
		{
			double real_x = freqToX(freq);
			background->drawLine(VSTGUI::CPoint(real_x, 0), VSTGUI::CPoint(real_x, height));
			if (freq == inc * 10) {
				inc *= 10;
			}
		}

		for (int db = dbMax - std::fmod(dbMax, 10); db >= dbMin; db -= 10)
		{
			double y = dBToY(db - dbMax);
			background->drawLine(VSTGUI::CPoint(0, y), VSTGUI::CPoint(width, y));
		}
		background->endDraw();
	}

	void Spectrum::calculateXCoords()
	{
		double freqBin = sampleRate / dataSize;
		if (xCoords)
			delete[] xCoords;
		xCoords = new double[dataSize >> 1];
		for (auto i = 0; i < (dataSize >> 1); i++)
		{
			//Make points be in the middle of frequency bins
			double freq = (i)*sampleRate / (dataSize)+freqBin / 2;
			xCoords[i] = freqToX(freq);
		}
	}

	double Spectrum::freqToX(double freq)
	{
		double x0 = log10(freqMin);
		return (log10(freq) - x0) * logScale;
	}

	double Spectrum::dBToY(double dB)
	{
		double dbScale = -getHeight() / (dbMax - dbMin);
		return dB * dbScale;
	}

	double Spectrum::complexToY(fftw_complex c)
	{
		double absol = sqrt(c[0] * c[0] + c[1] * c[1]);
		double db = 20 * log10(absol / (dataSize >> 1));
		double dbScale = getHeight() / (dbMax - dbMin);
		return (dbMax - db) * dbScale;
	}

	void Spectrum::drawChannel(unsigned int channel)
	{
	}

	std::vector<VSTGUI::CPoint> Spectrum::calculatePointsDep(fftw_complex* buffer)
	{
		std::vector<VSTGUI::CPoint> outVector;

		double width = getWidth();
		double height = getHeight();

		double dbScale = height / (dbMax - dbMin);

		double lastX = 0.0;

		VSTGUI::CPoint minp(0.0, 1000000.);

		std::lock_guard<std::mutex> lk(*mutex);

		bool first_point_in_range = true;
		for (int i = 0; i < dataSize >> 1; i++)
		{
			//double cabs = sqrt(buffer[i].real * buffer[i].real + buffer[i].imag * buffer[i].imag);
			double cabs = sqrt(buffer[i][0] * buffer[i][0] + buffer[i][1] * buffer[i][1]);
			double db = 20 * log10(cabs / (dataSize >> 1));
			double y = (dbMax - db) * dbScale;
			VSTGUI::CPoint p((int)xCoords[i], y); //we want one point per pixel so we will make sure to only use integer part of x coord

			// OPTION 1: Only Highest dB per 1 pixel
#pragma region Option1
			//if (outVector.empty())
			//{
			//	outVector.push_back(p);
			//}
			//else
			//{
			//	if (p.x == outVector.back().x)
			//	{
			//		if (p.y < minp.y) {
			//			//minp = p;
			//			outVector.pop_back();
			//			outVector.push_back(p);
			//			minp = p;
			//		}
			//	}
			//	else
			//	{
			//		outVector.push_back(minp);
			//		lastX = p.x;
			//		minp = p;
			//	}
			//}
#pragma endregion
			// OPTION 2: Highest dB per X pixels
#pragma region Option2
			if (outVector.empty())
			{
				outVector.push_back(p);
				lastX = p.x;
			}
			else
			{
				if (p.x < lastX + 5)
				{
					if (p.y < minp.y) {
						minp = p;
					}
					first_point_in_range = false;
				}
				else
				{
					if (first_point_in_range)
						outVector.push_back(p);
					else
						outVector.push_back(minp);
					lastX = p.x;
					minp = p;
					first_point_in_range = true;
				}
			}
#pragma endregion
		}
		return outVector;
	}

	std::vector<VSTGUI::CPoint> Spectrum::calculatePoints(fftw_complex* buffer)
	{
		std::vector<VSTGUI::CPoint> outVector;
		std::lock_guard<std::mutex> lk(*mutex);
		for (int i = 0; i < dataSize >> 1; i++)
		{
			VSTGUI::CPoint p((int)xCoords[i], complexToY(buffer[i]));
			outVector.push_back(p);
		}
		return outVector;
	}

	std::vector<VSTGUI::CPoint> Spectrum::filterVisible(std::vector<VSTGUI::CPoint> points)
	{
		std::vector<VSTGUI::CPoint> outVector;
		bool inRange = false;
		for (int i = 0; i < points.size(); i++)
		{
			if (inRange)
			{
				outVector.push_back(points[i]);
				if (points[i].x > getWidth())
				{
					//This is the first point out of view on the right side
					break;
				}
			}
			else
			{
				if (points[i].x >= 0 && points[i].x <= getWidth())
				{
					inRange = true;
					//This is the first point in view
					if (i > 0)
						outVector.push_back(points[i - 1]);
					outVector.push_back(points[i]);
				}
			}
		}
		return outVector;
	}

	void Spectrum::generateMaxLine(std::vector<CPoint> points, CGraphicsPath* path)
	{
		if (points.size() < 3) return;
		path->beginSubpath(points[0]);
		VSTGUI::CPoint previousControl(points[0]);
		previousControl.x += (points[1] - points[0]).x / 3;

		for (int i = 2; i < points.size(); i++) {
			//Take 3 points
			VSTGUI::CPoint p0(points[i - 2]);
			VSTGUI::CPoint p1(points[i - 1]);
			VSTGUI::CPoint p2(points[i]);
			//Calculate the direction change on the middle point
			double dir1 = p1.y - p0.y;
			double dir2 = p2.y - p1.y;

			VSTGUI::CPoint ctrl1, ctrl2;


			//Case 1: Direction is chaning, control points should be "flat"
			if (signbit(dir1) != signbit(dir2))
			{
				ctrl1 = ctrl2 = p1;
				ctrl1.x -= (p1 - p0).x / 3;
				ctrl2.x += (p2 - p1).x / 3;
			}
			else
			{
				//Case 2: Direction is the same, make smooth line 
				VSTGUI::CPoint d = p2 - p0;
				ctrl1 = p1 - d / 6;
				ctrl2 = p1 + d / 6;
			}
			path->addBezierCurve(previousControl, ctrl1, p1);
			previousControl = ctrl2;
		}
		//We need to add one last point
		VSTGUI::CPoint last(points.back());
		VSTGUI::CPoint prev(points[points.size() - 2]);
		VSTGUI::CPoint ctrl = last;
		ctrl.x -= (last - prev).x / 3;
		path->addBezierCurve(previousControl, ctrl, last);
	}

	void Spectrum::generateAccurateGraphicsPath(std::vector<CPoint> points, CGraphicsPath* path)
	{
		if (points.size() < 3) return;
		path->beginSubpath(points[0]);
		VSTGUI::CPoint previousControl(points[0]);
		previousControl.x += (points[1] - points[0]).x / 3;

		for (int i = 2; i < points.size(); i++) {
			//Take 3 points
			VSTGUI::CPoint p0(points[i - 2]);
			VSTGUI::CPoint p1(points[i - 1]);
			VSTGUI::CPoint p2(points[i]);
			//Calculate the direction change on the middle point
			double dir1 = p1.y - p0.y;
			double dir2 = p2.y - p1.y;

			VSTGUI::CPoint ctrl1, ctrl2;


			//Case 1: Direction is chaning, control points should be "flat"
			if (signbit(dir1) != signbit(dir2))
			{
				ctrl1 = ctrl2 = p1;
				ctrl1.x -= (p1 - p0).x / 3;
				ctrl2.x += (p2 - p1).x / 3;
			}
			else
			{
				//Case 2: Direction is the same, make smooth line 
				VSTGUI::CPoint d = p2 - p0;
				ctrl1 = p1 - d / 6;
				ctrl2 = p1 + d / 6;
			}
			path->addBezierCurve(previousControl, ctrl1, p1);
			previousControl = ctrl2;
		}
		//We need to add one last point
		VSTGUI::CPoint last(points.back());
		VSTGUI::CPoint prev(points[points.size() - 2]);
		VSTGUI::CPoint ctrl = last;
		ctrl.x -= (last - prev).x / 3;
		path->addBezierCurve(previousControl, ctrl, last);
	}

	void Spectrum::generateLocalMaximaGraphicsPath(std::vector<CPoint>, CGraphicsPath* path)
	{
	}

	void Spectrum::generateRoundedGraphicsPath(std::vector<CPoint>, CGraphicsPath* path, float threshold)
	{
	}

	void Spectrum::generateAccurateGraphicsPath(fftw_complex* buffer, CGraphicsPath* path)
	{
		double width = getWidth();
		double height = getHeight();
		double dbScale = height / (dbMax - dbMin);
		auto y = [dbScale, this](double cabs) {
			double db = 20 * log10(cabs / (dataSize >> 1));
			return (dbMax - db) * dbScale;
		};
		std::lock_guard<std::mutex> lk(*mutex);
		path->beginSubpath(VSTGUI::CPoint(0, height));
		path->addLine(VSTGUI::CPoint(xCoords[0], y(comp_abs(buffer[0]))));
		VSTGUI::CPoint previousControl(xCoords[0] + (xCoords[1] - xCoords[0]) / 3, y(comp_abs(buffer[0])));
		for (auto i = 2; i < dataSize >> 1; i++)
		{
			//Take 3 points
			VSTGUI::CPoint p0(xCoords[i - 2], y(comp_abs(buffer[i - 2])));
			VSTGUI::CPoint p1(xCoords[i - 1], y(comp_abs(buffer[i - 1])));
			VSTGUI::CPoint p2(xCoords[i], y(comp_abs(buffer[i])));
			//Calculate the direction change on the middle point
			double dir1 = p1.y - p0.y;
			double dir2 = p2.y - p1.y;

			VSTGUI::CPoint ctrl1, ctrl2;
			double dx = xCoords[i] - xCoords[i - 1];
			dx = dx / 3;

			//Case 1: Direction is chaning, control points should be "flat"
			if (signbit(dir1) != signbit(dir2))
			{
				ctrl1 = ctrl2 = p1;
				ctrl1.x -= dx;
				ctrl2.x += dx;
			}
			else
			{
				//Case 2: Direction is the same, make smooth line 
				VSTGUI::CPoint d = p2 - p1;
				ctrl1 = p1 - d / 6;
				ctrl2 = p1 + d / 6;
			}
			path->addBezierCurve(previousControl, ctrl1, p1);
			previousControl = ctrl2;
		}
		path->addLine(VSTGUI::CPoint(width, height));
		path->closeSubpath();
	}

#ifdef REFRESH_ANIMATION
	Spectrum::LoopAnimationTiming::LoopAnimationTiming() : TimingFunctionBase(1) {};
#endif // REFRESH_ANIMATION


}