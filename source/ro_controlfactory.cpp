#pragma once

#include <map>

#include "vstgui/vstgui.h"
#include "vstgui/vstgui_uidescription.h"
#include "vstgui/uidescription/detail/uiviewcreatorattributes.h"

#include "../include/ro_spectrumview.h"


#ifdef _DEBUG
#define new new( _NORMAL_BLOCK, __FILE__, __LINE__)
#endif
namespace VSTGUI {
	class RandOhmControlFactory : public ViewCreatorAdapter
	{
	private:
		std::map <std::string, AttrType> attrTypeMapping;
		std::map <std::string, void(RandOhm::Spectrum::*)(float)> floatAttrMapping;
		std::map <std::string, void(RandOhm::Spectrum::*)(VSTGUI::CColor)> colorAttrMapping;
		std::map <std::string, void(RandOhm::Spectrum::*)(Steinberg::int32)> tagAttrMapping;

	public:
		//register this class with the view factory
		RandOhmControlFactory() {
			attrTypeMapping["db-min"] = kFloatType;
			attrTypeMapping["db-max"] = kFloatType;
			attrTypeMapping["freq-min"] = kFloatType;
			attrTypeMapping["freq-max"] = kFloatType;

			attrTypeMapping["background-color"] = kColorType;
			attrTypeMapping["grid-color"] = kColorType;
			attrTypeMapping["channel1-color"] = kColorType;
			attrTypeMapping["channel2-color"] = kColorType;

			attrTypeMapping["bottom-db-tag"] = kTagType;
			attrTypeMapping["top-db-tag"] = kTagType;
			attrTypeMapping["bottom-freq-tag"] = kTagType;
			attrTypeMapping["top-freq-tag"] = kTagType;


			floatAttrMapping["db-min"] = &RandOhm::Spectrum::setMax;
			floatAttrMapping["db-max"] = &RandOhm::Spectrum::setMax;
			floatAttrMapping["freq-min"] = &RandOhm::Spectrum::setMax;
			floatAttrMapping["freq-max"] = &RandOhm::Spectrum::setMax;

			floatAttrMapping["background-color"] = &RandOhm::Spectrum::setMax;
			floatAttrMapping["grid-color"] = &RandOhm::Spectrum::setMax;
			floatAttrMapping["channel1-color"] = &RandOhm::Spectrum::setMax;
			floatAttrMapping["channel2-color"] = &RandOhm::Spectrum::setMax;


			UIViewFactory::registerViewCreator(*this);
		}

		IdStringPtr getViewName() const { return "RandOhm Spectrum"; }
		IdStringPtr getBaseViewName() const { return UIViewCreator::kCView; }

		CView* create(const UIAttributes& attributes, const IUIDescription* description) const
		{
			CRect size(CPoint(0, 0), CPoint(200, 200));
			return new RandOhm::Spectrum(size);
		}

		bool apply(CView* view, const UIAttributes& attributes,
			const IUIDescription* description) const override
		{

			return true;
		}

		bool getAttributeNames(StringList& attributeNames) const override {
			for (auto it = floatAttrMapping.begin(); it != floatAttrMapping.end(); it++) {
				attributeNames.push_back((*it).first);
			}
			attributeNames.push_back("test-attribute");
			return true;
		}

		AttrType getAttributeType(const string& attributeName) const override {
			if (attrTypeMapping.count(attributeName)) {
				return attrTypeMapping.at(attributeName);
			}
			return kUnknownType;
		}

		bool getAttributeValue(CView* view, const string& attributeName, string& stringValue,
			const IUIDescription* desc) const override
		{
			return false;
		}

		bool getPossibleListValues(const string& attributeName,
			ConstStringPtrList& values) const override
		{
			return false;
		}

		bool getAttributeValueRange(const string& attributeName, double& minValue,
			double& maxValue) const override
		{
			return false;
		}

	};

	RandOhmControlFactory __gMyControlFactory;
}
