//------------------------------------------------------------------------
// Copyright(c) 2021 randOhm.
//------------------------------------------------------------------------

#include "../include/ro_controller.h"
#include "../include/ro_cids.h"
#include "vstgui/plugin-bindings/vst3editor.h"
#include <vector>

#ifdef _DEBUG
#define new new( _NORMAL_BLOCK, __FILE__, __LINE__)
#endif
using namespace Steinberg;

namespace RandOhm {

	//------------------------------------------------------------------------
	// RandDelayController Implementation
	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumController::initialize(FUnknown* context)
	{
		// Here the Plug-in will be instanciated
		//VSTGUI::CView::kDirtyCallAlwaysOnMainThread = false;
		//---do not forget to call parent ------
		tresult result = EditControllerEx1::initialize(context);
		if (result != kResultOk)
		{
			return result;
		}
		pDbMin = RAND_DBMIN_DEF;
		pDbMax = RAND_DBMAX_DEF;
		pFreqMin = RAND_FREQMIN_DEF;
		pFreqMax = RAND_FREQMAX_DEF;
		// Here you could register some parameters
		uiParameters.addParameter(new Steinberg::Vst::RangeParameter(STR16("DB Min"), ParameterID::DB_MIN, STR16("db"), RAND_DBRANGE_MIN, RAND_DBRANGE_MAX, pDbMin));
		uiParameters.addParameter(new Steinberg::Vst::RangeParameter(STR16("DB Max"), ParameterID::DB_MAX, STR16("db"), RAND_DBRANGE_MIN, RAND_DBRANGE_MAX, pDbMax));
		uiParameters.addParameter(new Steinberg::Vst::RangeParameter(STR16("FQ Min"), ParameterID::FREQ_MIN, STR16("Hz"), RAND_FREQRANGE_MIN, RAND_FREQRANGE_MAX, pFreqMin));
		uiParameters.addParameter(new Steinberg::Vst::RangeParameter(STR16("FQ Max"), ParameterID::FREQ_MAX, STR16("Hz"), RAND_FREQRANGE_MIN, RAND_FREQRANGE_MAX, pFreqMax));

		Steinberg::Vst::StringListParameter *bufferSizeParam= new Steinberg::Vst::StringListParameter(STR16("BufferSize"), ParameterID::BUFFER_SIZE);
		bufferSizeParam->appendString(STR16("512"));
		bufferSizeParam->appendString(STR16("1024"));
		bufferSizeParam->appendString(STR16("2048"));
		bufferSizeParam->appendString(STR16("4096"));
		bufferSizeParam->appendString(STR16("8192"));
		uiParameters.addParameter(bufferSizeParam);

		//TODO: Should this be here?
		CView::idleRate = 60;

		return result;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumController::terminate()
	{
		// Here the Plug-in will be de-instanciated, last possibility to remove some memory!

		//---do not forget to call parent ------
		return EditControllerEx1::terminate();
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumController::setComponentState(IBStream* state)
	{
		// Here you get the state of the component (Processor part)
		if (!state)
			return kResultFalse;

		return kResultOk;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumController::setState(IBStream* state)
	{
		// Here you get the state of the controller

		return kResultTrue;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumController::getState(IBStream* state)
	{
		// Here you are asked to deliver the state of the controller (if needed)
		// Note: the real state of your plug-in is saved in the processor

		return kResultTrue;
	}

	//------------------------------------------------------------------------
	IPlugView* PLUGIN_API RandSpectrumController::createView(FIDString name)
	{
		// Here the Host wants to open your editor (if you have one)
		if (FIDStringsEqual(name, Vst::ViewType::kEditor))
		{
			// create your editor here and return a IPlugView ptr of it
			auto* view = new VSTGUI::VST3Editor(this, "view", "ro_editor.uidesc");
#ifdef REFRESH_THREAD_SETDIRTY
			view->setIdleRate(10); //this is needed if we want to use setDirty from a different thread to update view
#endif // REFRESH_THREAD_SETDIRTY
			return view;
		}
		return nullptr;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumController::setParamNormalized(Vst::ParamID tag, Vst::ParamValue value)
	{
		// called by host to update your parameters
		Steinberg::Vst::Parameter* parameter = parameters.getParameter(tag);
		if (parameter)
		{
			parameter->setNormalized(value);
		}
		else if (parameter = uiParameters.getParameter(tag))
		{
			Steinberg::Vst::ParamValue val = parameter->toPlain(value);
			auto verify = [](Vst::ParamValue min, Vst::ParamValue max) {return min < max; };
			switch (tag) {
			case ParameterID::DB_MIN:
				if (!verify(val, pDbMax)) return kResultFalse;
				pDbMin = val; break;
			case ParameterID::DB_MAX:
				if (!verify(pDbMin, val)) return kResultFalse;
				pDbMax = val; break;
			case ParameterID::FREQ_MIN:
				if (!verify(val, pFreqMax)) return kResultFalse;
				pFreqMin = val; break;
			case ParameterID::FREQ_MAX:
				if (!verify(pFreqMin, val)) return kResultFalse;
				pFreqMax = val; break;
			}
			parameter->setNormalized(value);
			view->setDbRange(pDbMin, pDbMax);
			view->setFrequencyRange(pFreqMin, pFreqMax);
			view->rescaleLevels();
		}
		return kResultTrue;
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumController::getParamStringByValue(Vst::ParamID tag, Vst::ParamValue valueNormalized, Vst::String128 string)
	{
		// called by host to get a string for given normalized value of a specific parameter
		// (without having to set the value!)
		if (Steinberg::Vst::Parameter* p = uiParameters.getParameter(tag))
		{
			p->toString(valueNormalized, string);
			return kResultTrue;
		}
		return EditControllerEx1::getParamStringByValue(tag, valueNormalized, string);
	}

	//------------------------------------------------------------------------
	tresult PLUGIN_API RandSpectrumController::getParamValueByString(Vst::ParamID tag, Vst::TChar* string, Vst::ParamValue& valueNormalized)
	{
		// called by host to get a normalized value from a string representation of a specific parameter
		// (without having to set the value!)

		return EditControllerEx1::getParamValueByString(tag, string, valueNormalized);
	}

	//------------------------------------------------------------------------
	VSTGUI::CView* RandSpectrumController::verifyView(VSTGUI::CView* view, const VSTGUI::UIAttributes& attributes, const VSTGUI::IUIDescription* description, VSTGUI::VST3Editor* editor)
	{
		if (auto spectrum = dynamic_cast<Spectrum*>(view))
		{
			this->view = spectrum;
			spectrum->setData(fftBuffer[0], fftBuffer[1], dataSize);
			spectrum->setMutex(mutex);
			spectrum->setSampleRate(sampleRate);
			spectrum->rescaleLevels();
		}
		return view;
	}

	//------------------------------------------------------------------------
	bool RandSpectrumController::isPrivateParameter(const Steinberg::Vst::ParamID paramID)
	{
		return uiParameters.getParameter(paramID) != 0 ? true : false;
	}

	//------------------------------------------------------------------------
	Steinberg::Vst::Parameter* RandSpectrumController::getParameterObject(Steinberg::Vst::ParamID tag)
	{
		Vst::Parameter* param = EditController::getParameterObject(tag);
		if (param == 0)
		{
			param = uiParameters.getParameter(tag);
		}
		return param;
	}

	//------------------------------------------------------------------------
	Steinberg::tresult RandSpectrumController::beginEdit(Steinberg::Vst::ParamID tag)
	{
		if (EditController::getParameterObject(tag))
			return EditController::beginEdit(tag);
		return kResultFalse;
	}

	//------------------------------------------------------------------------
	Steinberg::tresult RandSpectrumController::performEdit(Steinberg::Vst::ParamID tag, Steinberg::Vst::ParamValue valueNormalized)
	{
		if (EditController::getParameterObject(tag))
			return EditController::performEdit(tag, valueNormalized);
		return kResultFalse;
	}

	//------------------------------------------------------------------------
	Steinberg::tresult RandSpectrumController::endEdit(Steinberg::Vst::ParamID tag)
	{
		if (EditController::getParameterObject(tag))
			return EditController::endEdit(tag);
		return kResultFalse;
	}

	//------------------------------------------------------------------------
	Steinberg::tresult PLUGIN_API RandSpectrumController::notify(Steinberg::Vst::IMessage* message)
	{
		if (strcmp(message->getMessageID(), "data") == 0)
		{
			const void* ptr;
			Steinberg::uint32 ptrSize = 0;
			Steinberg::int64 i = 0;
			if (message->getAttributes()->getInt("LChannel", i) == kResultOk)
			{
				fftBuffer[0] = (RingBuffer<Steinberg::Vst::Sample32>*)i;
			}
			if (message->getAttributes()->getInt("RChannel", i) == kResultOk)
			{
				fftBuffer[1] = (RingBuffer<Steinberg::Vst::Sample32>*)i;
			}
			if (message->getAttributes()->getInt("Mutex", i) == kResultOk)
			{
				mutex = (std::mutex*)i;
			}
			if (message->getAttributes()->getInt("DataSize", dataSize) == kResultOk)
			{

			}
			if (message->getAttributes()->getFloat("SampleRate", sampleRate) == kResultOk)
			{

			}

			return kResultTrue;
		}
		return  EditControllerEx1::notify(message);
	}

	//------------------------------------------------------------------------
} // namespace RandOhm
