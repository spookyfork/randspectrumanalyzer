#include "../include/ro_fft.h"

#include <exception>
#include <cmath>

#include "../pluginterfaces/base/ftypes.h"
#include "../pluginterfaces/base/fstrdefs.h"


#ifndef M_PI
#define M_PI        3.14159265358979323846264338327950288
#endif


//TODO: Maybe do not rely on the external gPath here and instead pass the path in loadDLL??
//the way it is now will only be an issue if we try to call loadDLL() before the DllMain()
//so any sort of global instance of FFTWWrapper() causes issues
extern Steinberg::tchar gPath[];

//TODO: CAuses issues because it is called before the Dll init so we dont have the gPath
//RandOhm::FFTWWrapper* RandOhm::FFTWWrapper::instance = new RandOhm::FFTWWrapper();
RandOhm::FFTWWrapper* RandOhm::FFTWWrapper::instance = nullptr;
HINSTANCE RandOhm::FFTWWrapper::hDLL = 0;
RandOhm::FFTWWrapper::fftw_plan_dft_1d_t RandOhm::FFTWWrapper::fftw_plan_dft_1d = nullptr;
RandOhm::FFTWWrapper::fftw_execute_t RandOhm::FFTWWrapper::fftw_execute = nullptr;
RandOhm::FFTWWrapper::fftw_malloc_t RandOhm::FFTWWrapper::fftw_malloc = nullptr;
RandOhm::FFTWWrapper::fftw_free_t RandOhm::FFTWWrapper::fftw_free = nullptr;
RandOhm::FFTWWrapper::fftw_destroy_plan_t RandOhm::FFTWWrapper::fftw_destroy_plan = nullptr;
unsigned int RandOhm::FFTWWrapper::users = 0;


RandOhm::FFT::FFT(unsigned int bufferSize)
{
	this->bufferSize = bufferSize;
	in = (fftw_complex*)FFTWWrapper::fftw_malloc(sizeof(fftw_complex) * bufferSize);
	out = (fftw_complex*)FFTWWrapper::fftw_malloc(sizeof(fftw_complex) * bufferSize);
	p = FFTWWrapper::fftw_plan_dft_1d(bufferSize, in, out, FFTW_FORWARD, FFTW_PATIENT);
}

RandOhm::FFT::~FFT()
{
	FFTWWrapper::fftw_destroy_plan(p);
	FFTWWrapper::fftw_free(in);
	FFTWWrapper::fftw_free(out);
}

void RandOhm::FFT::execute()
{
	FFTWWrapper::fftw_execute(p); /* repeat as needed */
}

void RandOhm::FFT::execute(const RingBuffer<float>& f)
{
	fill(f);
	execute();
}

void RandOhm::FFT::execute(const RingBuffer<double>& f)
{
	fill(f);
	execute();
}

void RandOhm::FFT::put(unsigned int i, double sample)
{
	if (i >= bufferSize) throw new std::exception("out of bounds");
	in[i][0] = sample;
	in[i][1] = 0.0;
}

void RandOhm::FFT::fill(const RingBuffer<float>& f)
{
	if (f.getSize() != bufferSize) throw new std::exception("sizes are not the same"); //TODO: handle that somewhere or change it 
	for (auto i = 0; i < bufferSize; ++i)
	{
		in[i][0] = f.get(i);
		in[i][1] = 0.0;
	}
}

void RandOhm::FFT::fill(const RingBuffer<double>& f)
{
	if (f.getSize() != bufferSize) throw new std::exception("sizes are not the same"); //TODO: handle that somewhere or change it 
	for (auto i = 0; i < bufferSize; ++i)
	{
		in[i][0] = f.get(i);
		in[i][1] = 0.0;
	}
}

void RandOhm::FFT::getAbsolute(double* outBuffer)
{
	if (!outBuffer) throw new std::exception("out buffer is null"); //TODO: handle that somewhere or change it 
	for (auto i = 0; i < bufferSize; ++i)
	{
		outBuffer[i] = sqrt(out[i][0] * out[i][0] + out[i][1] * out[i][1]);
	}
}

void RandOhm::FFT::run_fft(int i, int N, int s = 1)
{
	//TODO: Maybe make a 3rd buffer that only gets updated at the very end and 
	//is edited with mutex to avoid data used for calculations to appear on screen
	if (N <= 1)
	{
		//in[i][0] = samples.get(i);
		//in[i][1] = 0.0;
		return;
	}
	else {
		//run_fft(i, N >> 1, s << 1);
		//run_fft(i + s, N >> 1, s << 1);
		//for (int k = 0; k < (N >> 1); k++) {
		//	int even_index = i + (k << 1) * s;
		//	int odd_index = even_index + s;
		//	double e_r = cos(-2.0 * M_PI * k / N);
		//	double e_i = sin(-2.0 * M_PI * k / N);
		//	double p[2] = { e_r * in[odd_index][0] - e_i * in[odd_index][1],
		//		e_r * in[odd_index][1] + e_i * in[odd_index][0] };
		//	double q[2] = { in[even_index][0], in[even_index][1] };
		//	in2[k][0] = q[0] + p[0];
		//	in2[k][1] = q[1] + p[1];
		//	in2[k + (N >> 1)][0] = q[0] - p[0];
		//	in2[k + (N >> 1)][1] = q[1] - p[1];
		//}
		//for (int k = 0; k < N; k++)
		//{
		//	in[i + k * s][0] = in2[k][0];
		//	in[i + k * s][1] = in2[k][1];
		//}
	}
}

RandOhm::FFTWWrapper::FFTWWrapper()
{
	if (!loadDll())
		throw new std::exception("Cannot load DLL");
}

RandOhm::FFTWWrapper::~FFTWWrapper()
{
	unloadDll();
}

bool RandOhm::FFTWWrapper::loadDll()
{
	users++;
	if (hDLL != 0) return true; //Already Loaded
	Steinberg::char16* s = new Steinberg::char16[Steinberg::strlen16(gPath) + 20];
	Steinberg::strcpy16(s, gPath);
	Steinberg::strcat16(s, STR16("libfftw3-3.dll"));
	hDLL = LoadLibrary(s);
	delete[] s;
	if (hDLL == 0) return false;

	// Get te functions we are going to be using
	fftw_plan_dft_1d = (fftw_plan_dft_1d_t)GetProcAddress(hDLL, "fftw_plan_dft_1d");
	fftw_execute = (fftw_execute_t)GetProcAddress(hDLL, "fftw_execute");
	fftw_malloc = (fftw_malloc_t)GetProcAddress(hDLL, "fftw_malloc");
	fftw_free = (fftw_free_t)GetProcAddress(hDLL, "fftw_free");
	fftw_destroy_plan = (fftw_destroy_plan_t)GetProcAddress(hDLL, "fftw_destroy_plan");

	return true;
}

bool RandOhm::FFTWWrapper::unloadDll()
{
	bool result = true;
	users--;
	if (hDLL != 0 && users == 0)
	{
		result = FreeLibrary(hDLL);
		hDLL = 0;
	}
	return result;
}
