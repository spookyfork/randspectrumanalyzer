//------------------------------------------------------------------------
// Copyright(c) 2021 randOhm.
//------------------------------------------------------------------------

#pragma once

#include <mutex>
#include <condition_variable>

#include "pluginterfaces/base/funknown.h"
#include "pluginterfaces/vst/vsttypes.h"

namespace RandOhm {
//------------------------------------------------------------------------
static const Steinberg::FUID kRandSpectrumProcessorUID(0xFC73AFAE, 0x693F5573, 0xBD47DE5E, 0x27959EAD);
static const Steinberg::FUID kRandSpectrumControllerUID (0x019F4937, 0xAFF355CA, 0xA5EF4680, 0xAFFFE042);

//! Do not declare variables here, unless they are going to be const
//! multiple instances of this plugin will use that same variable
//! For instance: mutex declared here to synchronize controller with processor
//! will be used by all instances of the plugin even though they do not share data

#define RandSpectrumAnalyzerVST3Category "Fx|Analyzer"

#define RAND_DBMIN_DEF -60.
#define RAND_DBMAX_DEF 0.
#define RAND_FREQMIN_DEF 20.0
#define RAND_FREQMAX_DEF 20000.

#define RAND_DBRANGE_MIN -100.
#define RAND_DBRANGE_MAX 10.
#define RAND_FREQRANGE_MIN 10.
#define RAND_FREQRANGE_MAX 24000.


/* Uncomment one of the following for refresh method */
//#define REFRESH_ONIDLE
//#define REFRESH_THREAD_SETDIRTY
#define REFRESH_TIMER
//#define REFRESH_ANIMATION

enum ParameterID : Steinberg::Vst::ParamID
{
	//EFFECT PARAMETERS
	BUFFER_SIZE = 101,
	METHOD = 102,
	WINDOWING = 103,


	//UI PARAMETERS
	DB_MIN = 1001,
	DB_MAX = 1002,
	FREQ_MIN = 1003,
	FREQ_MAX = 1004,
};



//------------------------------------------------------------------------
} // namespace RandOhm
