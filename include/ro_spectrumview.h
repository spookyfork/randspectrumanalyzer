#pragma once

#include <vector>
#include <chrono>
#include <thread>
#include <mutex>


#include "vstgui/vstgui.h"
#include "pluginterfaces/base/ftypes.h"
#include "pluginterfaces/vst/vsttypes.h"

#include "ro_fft.h"
#include "ro_cids.h"

#include "../lib/fftw3.h"

using namespace VSTGUI;

namespace RandOhm {
	class Spectrum : public CControl
	{
	public:

#ifdef REFRESH_ANIMATION
		class ViewDrawAnimation : public Animation::IAnimationTarget, public ReferenceCounted<int32_t>
		{
		public:
			// Inherited via IAnimationTarget
			void animationStart(CView* view, IdStringPtr name) override
			{
			}
			void animationTick(CView* view, IdStringPtr name, float pos) override
			{
				if (auto spec = dynamic_cast<Spectrum*>(view)) {
					spec->update();
				}
				//view->invalid(); //Refresh
			}
			void animationFinished(CView* view, IdStringPtr name, bool wasCanceled) override
			{
			}
		protected:

		};

		class LoopAnimationTiming : public VSTGUI::Animation::TimingFunctionBase
		{
		public:
			explicit LoopAnimationTiming();
			explicit LoopAnimationTiming(float loopTime) : TimingFunctionBase(1) { this->loopTime = loopTime; }
			// Inherited via TimingFunctionBase
			float getPosition(uint32_t milliseconds) override
			{
				float dt = (milliseconds - lastTime) / loopTime;
				lastTime = milliseconds;
				return dt;
			}
			bool isDone(uint32_t milliseconds) override
			{
				return false;
			};
		protected:
			uint32_t lastTime = 0;
			float loopTime = 1000.0;
		};
#endif // REFRESH_ANIMATION

		enum Mode {
			LINE,
			BARS
		};

		Spectrum(const CRect& size);
		~Spectrum();

#ifdef REFRESH_ONIDLE
		virtual void onIdle() override { update(); }
#endif // REFRESH_ONIDLE


		void draw(CDrawContext* pContext) override;
		bool attached(CView* parent) override;
		bool removed(CView* parent) override;
		void setViewSize(const CRect& rect, bool invalid = true) override;
		void update();

		void setData(RingBuffer<Steinberg::Vst::Sample32>* l, RingBuffer<Steinberg::Vst::Sample32>* r, unsigned int dataSize);
		void setSampleRate(double sampleRate);
		void setFrequencyRange(double fMin, double fMax);
		void setDbRange(double dbMin, double dbMax);
		void rescaleLevels();
		void setMutex(std::mutex* m) { mutex = m; }

		CLASS_METHODS(Spectrum, CControl);
	protected:
		typedef struct {
			fftw_complex* buffer;
			std::vector<CPoint> points;
			CColor strokeColor;
			CColor fillColor;
			bool hold;
			int mode;
		} ChannelInfo;

		typedef struct sstyle_t {

			const VSTGUI::CColor orange = VSTGUI::CColor(211, 132, 0, 255);
			const VSTGUI::CColor green = VSTGUI::CColor(0, 211, 0, 255);
			const VSTGUI::CColor claret = VSTGUI::CColor(130, 9, 51, 255);
			const VSTGUI::CColor pink = VSTGUI::CColor(216, 71, 151, 255);
			const VSTGUI::CColor cyan = VSTGUI::CColor(210, 253, 255, 255);
			const VSTGUI::CColor capri = VSTGUI::CColor(58, 190, 255, 255);
			const VSTGUI::CColor turquoise = VSTGUI::CColor(38, 255, 230, 255);

			const VSTGUI::CColor* channelColors[10] = {
				&orange,		//0
				&green,			//1
				&claret,		//2
				&pink,			//3
				&cyan,			//4
				&capri,			//5
				&turquoise,		//6
				&turquoise,		//7
				&turquoise,		//8
				&turquoise,		//9
			};

			VSTGUI::CColor backgroundColor;
			VSTGUI::CColor fontColor;
			VSTGUI::CColor gridColor;
			VSTGUI::CColor defaultColor; //If there is no info
			VSTGUI::CColor* strokeColor = nullptr;
			VSTGUI::CColor* fillColor = nullptr;
			int channels;

			VSTGUI::CFontDesc debugFont;

			sstyle_t(int channels = 2) {
				this->channels = channels;
				backgroundColor = CColor(25, 25, 25, 255);
				fontColor = CColor(255, 216, 152, 255);
				gridColor = CColor(117, 117, 117, 255);
				defaultColor = orange;
				setChannels(channels);
			};

			void setChannels(int channels) {
				if (strokeColor) delete[] strokeColor;
				if (fillColor) delete[] fillColor;

				strokeColor = new VSTGUI::CColor[channels];
				fillColor = new VSTGUI::CColor[channels];
				for (int i = 0; i < channels; i++) {
					strokeColor[i] = *channelColors[i];
					fillColor[i] = strokeColor[i];
					fillColor[i].red /= 2;
					fillColor[i].green /= 2;
					fillColor[i].blue /= 2;
					fillColor[i].alpha /= 2;
				}
			}

			VSTGUI::CColor getStrokeForChannel(int channel) {
				if (channel >= channels) {
					return defaultColor;
				}
				return strokeColor[channel];
			}

			VSTGUI::CColor getFillForChannel(int channel) {
				if (channel >= channels) {
					return defaultColor;
				}
				return fillColor[channel];
			}

		} SpectrumStyle;

		void drawChannel(unsigned int channel);

		//Change the buffer to a list of on-screen points
		std::vector<VSTGUI::CPoint> calculatePointsDep(fftw_complex* buffer);
		std::vector<VSTGUI::CPoint> calculatePoints(fftw_complex* buffer);
		//Filer only poins that are in view (+2 points that are outside on both sides so we can draw a line)
		std::vector<VSTGUI::CPoint> filterVisible(std::vector<VSTGUI::CPoint> points);
		//Generate Max Line from the calculated points
		void generateMaxLine(std::vector<CPoint>, CGraphicsPath* path);

		void generateAccurateGraphicsPath(std::vector<CPoint>, CGraphicsPath* path);
		void generateLocalMaximaGraphicsPath(std::vector<CPoint>, CGraphicsPath* path);
		void generateRoundedGraphicsPath(std::vector<CPoint>, CGraphicsPath* path, float threshold);
		void generateAccurateGraphicsPath(fftw_complex* buffer, CGraphicsPath* path);

		void generateBackground();
		void calculateXCoords();

		double freqToX(double freq);
		double dBToY(double dB);
		double comp_abs(fftw_complex c) { return sqrt(c[0] * c[0] + c[1] * c[1]); };
		double complexToY(fftw_complex c);

		//Background with a grid
		SharedPointer<VSTGUI::COffscreenContext> background = nullptr;

		double maxFreq = 24000.0;
		double sampleRate = 48000.0;
		double logScale;
		double* xCoords = nullptr;

		double dbMin, dbMax;
		double freqMin, freqMax;
		double falloffSpeed = 10.0; //pixels per second

		//Shared buffers with processor
		RingBuffer<Steinberg::Vst::Sample32>* inBuffers[2] = { nullptr, nullptr };
		FFT* fftB[2] = { nullptr, nullptr };
		fftw_complex* fftBuffer[2] = { nullptr,nullptr };
		unsigned int dataSize = 0;

		//Style
		SpectrumStyle style;

		//Debug variables
		float dt = 0.0;
		std::string debugText;

#ifdef REFRESH_THREAD_SETDIRTY
		std::thread* refreshThread = nullptr;
		bool threadShouldEnd = false;
#endif // REFRESH_THREAD_SETDIRTY

#ifdef REFRESH_TIMER
		CVSTGUITimer* timer = nullptr;
#endif // REFRESH_TIMER

		std::chrono::system_clock::time_point lastTime;
		std::chrono::system_clock::time_point currentTime;

		std::mutex* mutex;
	};

}