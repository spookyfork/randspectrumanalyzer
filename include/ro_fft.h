//------------------------------------------------------------------------
// Copyright(c) 2022 randOhm.
//------------------------------------------------------------------------
#pragma once
#include <thread>
#include <Windows.h>
#include "../lib/fftw3.h"

#include "../pluginterfaces/vst/vsttypes.h"

namespace RandOhm {
	class FFTWWrapper {
	public:
		//FFTW library functions
		typedef fftw_plan(*fftw_plan_dft_1d_t)(int n, fftw_complex* in, fftw_complex* out, int sign, unsigned flags);
		typedef fftw_plan(*fftw_plan_r2r_1d_t)(int n, double* in, double* out, fftw_r2r_kind kind, unsigned flags);
		typedef void (*fftw_execute_t)(const fftw_plan p);
		typedef void (*fftw_execute_r2r_t)(const fftw_plan p, double* in, double* out);
		typedef void* (*fftw_malloc_t)(size_t n);
		typedef void (*fftw_free_t)(void* p);
		typedef void (*fftw_destroy_plan_t)(fftw_plan p);

		static fftw_plan_dft_1d_t fftw_plan_dft_1d;
		static fftw_plan_r2r_1d_t fftw_plan_r2r_1d;
		static fftw_execute_t fftw_execute;
		static fftw_execute_r2r_t fftw_execute_r2r;
		static fftw_malloc_t fftw_malloc;
		static fftw_free_t fftw_free;
		static fftw_destroy_plan_t fftw_destroy_plan;

		static bool loadDll();
		static bool unloadDll();

	protected:
		FFTWWrapper();
		~FFTWWrapper();

		static unsigned int users; //TODO: Figure out multiple instances of of the plugin

		static FFTWWrapper* instance; //TODO: will this ever be needed?

		//DLL loading 
		static HINSTANCE hDLL;
	};

	template<typename T>
	class RingBuffer
	{
	public:
		RingBuffer(int size) : size(size) { buffer = new T[size]; memset(buffer, 0, sizeof(T) * size); }
		RingBuffer(const RingBuffer<T>& a) {
			size = a.size;
			buffer = new T[size]; 
			memcpy(buffer, a.buffer, sizeof(T) * size);
			index = a.index;
			start = a.start;
		};
		~RingBuffer() { delete[] buffer; }

		void setStart() { start = index; }
		int getSize() const { return size; }

		//push method for pointers (fftw_complex is a double[2], so we cant just do buffer[index] = val)
		template<typename T, typename std::enable_if_t<std::is_pointer<T>::value>* = nullptr>
		void push(T val) {
			memcpy(&buffer[index], val, sizeof(T));
			//buffer[index] = val;
			inc();
		}

		//push method for base types (float, double)
		template<typename T, typename std::enable_if_t<!std::is_pointer<T>::value>* = nullptr>
		void push(T val) {
			//memcpy(&buffer[index], &val, sizeof(T));
			buffer[index] = val;
			inc();
		}

		T& getAbsolute(int ind) { return buffer[ind]; }
		T& get(int ind) const { int tmp = ind - start; if (tmp < 0) tmp += size; return buffer[tmp]; }

		T& operator[](int i) { return get(i); };

	protected:
		void inc() { index++; if (index == size) index = 0; }

		T* buffer;
		int size;
		int start = 0;
		int index = 0;
	};
	/**
	* FIXME: This class can only be dynamically created after the FFTW was loaded
	* 
	* 1. Fill the in array with RingBuffer data
	* 2. execute()
	* 3. get absolute values
	*/
	class FFT {
	public:
		FFT(unsigned int bufferSize);
		~FFT();
		void execute();
		void execute(const RingBuffer<float>& f);
		void execute(const RingBuffer<double>& f);
		void put(unsigned int i, double sample);
		fftw_complex* getOutBuffer() { return out; }
		//void fill(RingBuffer<float> f);
		void fill(const RingBuffer<float>& f);
		void fill(const RingBuffer<double>& f);
		void getAbsolute(double* outBuffer);

	protected:
		[[deprecated("Use run_fft2() instead.")]]
		void run_fft(int i, int N, int s);

		fftw_complex* in;
		fftw_complex* out;
		fftw_plan p;
		unsigned int bufferSize;
	};
}