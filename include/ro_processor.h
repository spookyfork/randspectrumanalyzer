//------------------------------------------------------------------------
// Copyright(c) 2021 randOhm.
//------------------------------------------------------------------------

#pragma once

#include <thread>
#include <mutex>
#include <vector>
#include <complex>

#include "public.sdk/source/vst/vstaudioeffect.h"

#include "../lib/fftw3.h"

#include "../include/ro_fft.h"

#define M_PI        3.14159265358979323846264338327950288

#define FFT_BUFFER_SIZE 4096*2 //Maximum buffer size


#ifdef _DEBUG
#define new new( _NORMAL_BLOCK, __FILE__, __LINE__)
#endif

namespace RandOhm {
	//------------------------------------------------------------------------
	//  RandSpectrumProcessor
	//------------------------------------------------------------------------
	class RandSpectrumProcessor : public Steinberg::Vst::AudioEffect
	{
	public:
		RandSpectrumProcessor();
		~RandSpectrumProcessor() SMTG_OVERRIDE;

		// Create function
		static Steinberg::FUnknown* createInstance(void* /*context*/)
		{
			return (Steinberg::Vst::IAudioProcessor*)new RandSpectrumProcessor;
		}

		//--- ---------------------------------------------------------------------
		// AudioEffect overrides:
		//--- ---------------------------------------------------------------------
		/** Called at first after constructor */
		Steinberg::tresult PLUGIN_API initialize(Steinberg::FUnknown* context) SMTG_OVERRIDE;

		/** Called at the end before destructor */
		Steinberg::tresult PLUGIN_API terminate() SMTG_OVERRIDE;

		/** Switch the Plug-in on/off */
		Steinberg::tresult PLUGIN_API setActive(Steinberg::TBool state) SMTG_OVERRIDE;

		/** Will be called before any process call */
		Steinberg::tresult PLUGIN_API setupProcessing(Steinberg::Vst::ProcessSetup& newSetup) SMTG_OVERRIDE;

		/** Asks if a given sample size is supported see SymbolicSampleSizes. */
		Steinberg::tresult PLUGIN_API canProcessSampleSize(Steinberg::int32 symbolicSampleSize) SMTG_OVERRIDE;

		/** Here we go...the process call */
		Steinberg::tresult PLUGIN_API process(Steinberg::Vst::ProcessData& data) SMTG_OVERRIDE;

		/** For persistence */
		Steinberg::tresult PLUGIN_API setState(Steinberg::IBStream* state) SMTG_OVERRIDE;
		Steinberg::tresult PLUGIN_API getState(Steinberg::IBStream* state) SMTG_OVERRIDE;


		Steinberg::tresult PLUGIN_API setBusArrangements(Steinberg::Vst::SpeakerArrangement* inputs, Steinberg::int32 numIns,
			Steinberg::Vst::SpeakerArrangement* outputs, Steinberg::int32 numOuts) SMTG_OVERRIDE;

		/** Test of a communication channel between controller and component */
		//Steinberg::tresult receiveText(const char* text) SMTG_OVERRIDE;

		/** We want to receive message. */
		//Steinberg::tresult PLUGIN_API notify(Steinberg::Vst::IMessage* message) SMTG_OVERRIDE;

	//------------------------------------------------------------------------
	protected:
		RingBuffer<Steinberg::Vst::Sample32>* buffer32;

		unsigned int bufferSize = FFT_BUFFER_SIZE;
		unsigned int fftSize = FFT_BUFFER_SIZE;

		std::mutex ro_mutex;
	};

	//------------------------------------------------------------------------
} // namespace RandOhm