//------------------------------------------------------------------------
// Copyright(c) 2021 randOhm.
//------------------------------------------------------------------------

#pragma once

#include "vstgui/plugin-bindings/vst3editor.h"
#include "public.sdk/source/vst/vsteditcontroller.h"
#include "../include/ro_spectrumview.h"
#include "../include/ro_fft.h"

#include "../lib/fftw3.h"

namespace RandOhm {

//------------------------------------------------------------------------
//  RandDelayController
//------------------------------------------------------------------------
class RandSpectrumController : public Steinberg::Vst::EditControllerEx1, public VSTGUI::VST3EditorDelegate
{
public:
//------------------------------------------------------------------------
	RandSpectrumController () = default;
	~RandSpectrumController () SMTG_OVERRIDE = default;

    // Create function
	static Steinberg::FUnknown* createInstance (void* /*context*/)
	{
		return (Steinberg::Vst::IEditController*)new RandSpectrumController;
	}

	// IPluginBase
	Steinberg::tresult PLUGIN_API initialize (Steinberg::FUnknown* context) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API terminate () SMTG_OVERRIDE;

	// EditController
	Steinberg::tresult PLUGIN_API setComponentState (Steinberg::IBStream* state) SMTG_OVERRIDE;
	Steinberg::IPlugView* PLUGIN_API createView (Steinberg::FIDString name) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API setState (Steinberg::IBStream* state) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API getState (Steinberg::IBStream* state) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API setParamNormalized (Steinberg::Vst::ParamID tag,
                                                      Steinberg::Vst::ParamValue value) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API getParamStringByValue (Steinberg::Vst::ParamID tag,
                                                         Steinberg::Vst::ParamValue valueNormalized,
                                                         Steinberg::Vst::String128 string) SMTG_OVERRIDE;
	Steinberg::tresult PLUGIN_API getParamValueByString (Steinberg::Vst::ParamID tag,
                                                         Steinberg::Vst::TChar* string,
                                                         Steinberg::Vst::ParamValue& valueNormalized) SMTG_OVERRIDE;

	VSTGUI::CView* verifyView(VSTGUI::CView* view, const VSTGUI::UIAttributes& attributes, const VSTGUI::IUIDescription* description, VSTGUI::VST3Editor* editor) override;

	// EditController -- These are to support UI-only parameters stored in uiParameters
	virtual Steinberg::Vst::Parameter* getParameterObject(Steinberg::Vst::ParamID tag) SMTG_OVERRIDE;
	virtual Steinberg::tresult beginEdit(Steinberg::Vst::ParamID tag) SMTG_OVERRIDE;
	virtual Steinberg::tresult performEdit(Steinberg::Vst::ParamID tag, Steinberg::Vst::ParamValue valueNormalized) SMTG_OVERRIDE;
	virtual Steinberg::tresult endEdit(Steinberg::Vst::ParamID tag) SMTG_OVERRIDE;


	// VSTGUI::VST3EditorDelegate
	virtual bool isPrivateParameter(const Steinberg::Vst::ParamID paramID) SMTG_OVERRIDE;

	//VSTGUI::IController* createSubController(VSTGUI::UTF8StringPtr name, const VSTGUI::IUIDescription* description, VSTGUI::VST3Editor* editor) SMTG_OVERRIDE;
	//void willClose(VSTGUI::VST3Editor* editor);
	//void deleteSubController();

	/** Test of a communication channel between controller and component */
	//Steinberg::tresult receiveText(const char* text) SMTG_OVERRIDE;

	/** We want to receive message. */
	Steinberg::tresult PLUGIN_API notify(Steinberg::Vst::IMessage* message) SMTG_OVERRIDE;

	//---Interface---------
	DEFINE_INTERFACES
	// Here you can add more supported VST3 interfaces
	// DEF_INTERFACE (Vst::IXXX)
	END_DEFINE_INTERFACES(EditController)
	DELEGATE_REFCOUNT(EditController)
	
//------------------------------------------------------------------------
protected:
	Spectrum* view = nullptr;
	RingBuffer<Steinberg::Vst::Sample32>* fftBuffer[2] = { nullptr, nullptr };
	Steinberg::int64 dataSize = 0;
	double sampleRate = 0;
	Steinberg::Vst::ParamValue pDbMin, pDbMax, pFreqMin, pFreqMax;

	Steinberg::Vst::ParameterContainer uiParameters;

	std::mutex *mutex;
};

//------------------------------------------------------------------------
} // namespace RandOhm
