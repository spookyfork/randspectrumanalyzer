	class ComplexNumber {
	public:
		ComplexNumber() { ComplexNumber(0, 0); }
		ComplexNumber(double real, double imag) : real(real), imag(imag) {};
		ComplexNumber(ComplexNumber & a) : real(a.real), imag(a.imag) {};

		double abs() { return sqrt(real * real + imag * imag); }

		static ComplexNumber exp(double theta)
		{
			return ComplexNumber(cos(theta), sin(theta));
		}

		ComplexNumber operator+(const ComplexNumber& a) const {
			return ComplexNumber(real + a.real, imag + a.imag);
		}
		ComplexNumber operator*(const ComplexNumber& a) const {
			return ComplexNumber(real * a.real - imag * a.imag, real*a.imag + imag*a.real);
		}
		ComplexNumber operator-(const ComplexNumber& a) const {
			return ComplexNumber(real - a.real, imag - a.imag);
		}

		ComplexNumber& operator=(const ComplexNumber& a) {
			real = a.real;
			imag = a.imag;
			return *this;
		}

	//protected:
		double real, imag;
	};